#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import sys
import requests
from PIL import Image
from StringIO import StringIO
from ConfigParser import SafeConfigParser

class UploadVk(object):
    def __init__(self, url, message=None):
        self.__config_parser()
        self.__message = message
        self.__filename = self.__prepare(url)
        if self.__doc:
            __id = self.__upoload_doc()
        else:
            __id = self.__upload_img()
        self.output = self.__wall_post(__id)

    def __upload_img(self):
        url1 = 'https://api.vkontakte.ru/method/photos.getWallUploadServer'
        url2 = ''
        url3 = 'https://api.vkontakte.ru/method/photos.saveWallPhoto'
        #step_1
        payload = {
            'gid': self.__gid,
            'access_token': self.__access_token
        }
        try:
            data1 = requests.get(url1, params=payload)
        except requests.exceptions.RequestException:
            sys.stderr.write('Some network error.'
                ' Cannot upload image on step 1.\n')
            sys.exit(1)
        if data1.status_code == requests.codes.ok:
            payload.clear()
            json1 = data1.json()
            url2 = json1['response']['upload_url']
        else:
            sys.stderr.write('Vk api isn\'t available.\nStep 1 failed!\n')
            sys.exit(1)
        #step_2
        try:
            photo = {
                'photo': open(self.__filename, 'rb')
            }
        except IOError:
            sys.stderr.write('Cannot open image.\n')
            sys.exit(1)
        try:
            data2 = requests.post(url2, files=photo)
        except requests.exceptions.RequestException:
            sys.stderr.write('Some network error.'
                ' Cannot upload image on step 2.\n')
            sys.exit(1)
        del photo
        os.remove(self.__filename)
        if data2.status_code == requests.codes.ok:
            json2 = data2.json()
        else:
            sys.stderr.write('Vk api isn\'t available.\nStep 2 failed!\n')
            sys.exit(1)
        #step_3
        payload = {
            'server': json2['server'],
            'hash': json2['hash'],
            'photo': json2['photo'],
            'gid': self.__gid,
            'access_token': self.__access_token
        }
        try:
            data3 = requests.post(url3, params=payload)
        except requests.exceptions.RequestException:
            sys.stderr.write('Some network error.'
                ' Cannot upload image on step 3.\n')
            sys.exit(1)
        if data3.status_code == requests.codes.ok:
            payload.clear()
            json3 = data3.json()
        else:
            sys.stderr.write('Vk api isn\'t available.\nStep 3 failed!\n')
            sys.exit(1)
        return json3['response'][0]['id']

    def __upoload_doc(self):
        url1 = 'https://api.vkontakte.ru/method/docs.getUploadServer'
        url2 = ''
        url3 = 'https://api.vkontakte.ru/method/docs.save'
        #step_1
        payload = {
            'access_token': self.__access_token
        }
        try:
            data1 = requests.get(url1, params=payload)
        except requests.exceptions.RequestException:
            sys.stderr.write('Some network error.'
                ' Cannot upload document on step 1.\n')
            sys.exit(1)
        if data1.status_code == requests.codes.ok:
            json1 = data1.json()
            url2 = json1['response']['upload_url']
        else:
            sys.stderr.write('Vk api isn\'t available.\nStep 1 failed!\n')
            sys.exit(1)
        #step_2
        try:
            files = {
                'file': open(self.__filename, 'rb')
            }
        except IOError:
            sys.stderr.write('Cannot open document.\n')
            sys.exit(1)
        try:
            data2 = requests.post(url2, files=files)
        except requests.exceptions.RequestException:
            sys.stderr.write('Some network error.'
                ' Cannot upload document on step 2.\n')
            sys.exit(1)
        del files
        os.remove(self.__filename)
        if data2.status_code == requests.codes.ok:
            json2 = data2.json()
        else:
            sys.stderr.write('Vk api isn\'t available.\nStep 2 failed!\n')
            sys.exit(1)
        #step_3
        payload['file'] = json2['file']
        try:
            data3 = requests.post(url3, params=payload)
        except requests.exceptions.RequestException:
            sys.stderr.write('Some network error.'
                ' Cannot upload document on step 3.\n')
            sys.exit(1)
        if data3.status_code == requests.codes.ok:
            payload.clear()
            json3 = data3.json()
        else:
            sys.stderr.write('Vk api isn\'t available.\nStep 3 failed!\n')
            sys.exit(1)
        #returned data
        did = json3['response'][0]['did']
        owner_id = json3['response'][0]['owner_id']
        attach = 'doc%s_%s' % (owner_id, did)
        return attach

    def __wall_post(self, attach):
        """
        Posting object to wall
        """
        url = 'https://api.vkontakte.ru/method/wall.post'
        payload = {
            'access_token': self.__access_token,
            'owner_id': '-%s' % self.__gid,
            'attachments': attach,
            'from_group': 1
        }
        if not self.__message == None:
            payload['message'] = self.__message
        try:
            data = requests.post(url, params=payload)
        except requests.exceptions.RequestException:
            sys.stderr.write('Some network error. Cannot post on wall.\n')
            sys.exit(1)
        if data.status_code == requests.codes.ok:
            payload.clear()
            json = data.json()
        else:
            sys.stderr.write('Vk api isn\'t available.\nStep 4 failed!\n')
            sys.exit(1)
        post = 'http://vk.com/wall-%s_%s' % (self.__gid,
            json['response']['post_id'])
        return post

    def __prepare(self, img_url):
        """
        Prepare image or gif for upload to server
        """
        suffix = ['JPEG', 'PNG', 'GIF', 'BMP']
        match = re.search('\/([^/]+\.([^/]+))$', img_url)
        if match.group(1):
            file_name = '/tmp/%s' % match.group(1)
            try:
                img = requests.get(img_url)
            except requests.exceptions.RequestException:
                sys.stderr.write('Bad url\n')
                sys.exit(1)

            if img.status_code == requests.codes.ok:
                #TODO try IOError
                image = Image.open(StringIO(img.content))
            else:
                sys.stderr.write('Image doesn\'t download.\n')
                sys.exit(1)
        else:
            sys.stderr.write('Image url isn\'t valid\n')
            sys.exit(1)
        if image.format in suffix:
            if image.format == 'GIF' and image.info['version'] == 'GIF89a':
                self.__doc = True
                try:
                    f = open(file_name, 'w')
                    f.write(img.content)
                    f.close
                except IOError:
                    sys.stderr.write('Can\'t write gif file to /tmp.\n')
                    sys.exit(1)
            else:
                self.__doc = False
                try:
                    image.save(file_name)
                except IOError:
                    sys.stderr.write('Can\'t write image file to /tmp.\n')
                    sys.exit(1)
        else:
            sys.stderr.write('Bad image format.\n')
            sys.exit(1)
        return file_name

    def __config_parser(self):
        """
        Parse config file with possible error processing
        """
        parser = SafeConfigParser()
        try:
            parser.read('/etc/moodflight.conf')
            self.__gid = parser.get('main', 'gid')
            self.__access_token = parser.get('main', 'token')
        except ConfigParser.Error:
            sys.stderr.write('Can\'t parse config file.\n')
            sys.exit(1)

def main():
    if len(sys.argv) == 2:
        img_url = sys.argv[1]
    elif len(sys.argv) < 2:
        sys.stderr.write('Need image url as an argument.\n')
        sys.exit(1)
    elif len(sys.argv) > 2:
        sys.stderr.write('Too many arguments.\n')
        sys.exit(1)

    asuka = UploadVk(img_url)
    print asuka.output
    sys.exit(0)

if __name__ == '__main__':
    main()

